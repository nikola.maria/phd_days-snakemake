# Snakemake tutorial minimal example

## Getting started

Install miniconda as described [here](https://docs.conda.io/projects/miniconda/en/latest/). Choose the appropriate version (macOS or Linux).

For Mac users:

```         
mkdir -p ~/miniconda3
curl https://repo.anaconda.com/miniconda/Miniconda3-latest-MacOSX-arm64.sh -o ~/miniconda3/miniconda.sh
bash ~/miniconda3/miniconda.sh -b -u -p ~/miniconda3
rm -rf ~/miniconda3/miniconda.sh
```

For Linux users:

```         
mkdir -p ~/miniconda3
wget https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh -O ~/miniconda3/miniconda.sh
bash ~/miniconda3/miniconda.sh -b -u -p ~/miniconda3
rm -rf ~/miniconda3/miniconda.sh
```

Initialize conda for bash and zsh shells:

``` zsh
~/miniconda3/bin/conda init bash
~/miniconda3/bin/conda init zsh
```

Activate the base environment and create a snakemake environment:

``` zsh
conda create -c conda-forge -c bioconda -n snakemake snakemake
```

Activate the snakemake environment.

``` zsh
conda activate snakemake
```

## Minimal example - learning the syntax

Create a `minimal-example` directory and go to that directory:

``` zsh
mkdir minimal-example
cd minimal-example
```

Create a `data` directory.

``` zsh
mkdir data
```

Rules are written in a file called `Snakefile`

Create the `Snakefile` in your working directory with

``` zsh
nano Snakefile
```

We will keep the Snakefile empty for now.

Create a file called `threewords.txt` in your `data` directory. The file contains three words of your choice, each in a separate line.

``` zsh
nano data/threewords.txt
```

In our first rule, we want to count the characters in the file `data/threewords.txt` and write the output into a file called `characters.txt` in `data`.

``` zsh
wc -m data/threewords.txt > data/characters.txt
```

``` python
rule charactercount:
  input: "data/threewords.txt"
  output: "data/characters.txt"
  shell:
    """
    wc -m {input} > {output}
    """
```

We want to add a second rule that counts the lines in `data/threewords.txt` and write the output to a file called `lines.txt` in `data`.

``` zsh
wc -l data/threewords.txt > data/lines.txt
```

``` {.python .snakemake}
rule linecount:
  input: "data/threewords.txt"
  output: "data/lines.txt"
  shell:
    """
    wc -l {input} > {output}
    """
```

Now we want to combine both results, reading both files and writing the output to a third file called `summary.txt` in `data`.

``` python
rule summary:
  input:
    characters = "data/characters.txt",
    lines = "data/lines.txt"
  output: "data/summary.txt"
  shell:
    """
    cat {input.characters} > {output}
    cat {input.lines} >> {output}
    """
```

Add a rule to assure execution of all previous rules:

``` python
rule all:
  input: "data/summary.txt"
```

![](img/dag.png)

## Advanced example - ATAC-seq peak calling

The script is adapted from the first few steps of the [Galaxy atac-seq tutorial](https://training.galaxyproject.org/training-material/topics/epigenetics/tutorials/atac-seq/tutorial.html).

### Downloading files

``` shell
wget https://zenodo.org/record/3270536/files/SRR891268_R1.fastq.gz
wget https://zenodo.org/record/3270536/files/SRR891268_R2.fastq.gz
wget http://hgdownload.soe.ucsc.edu/goldenPath/hg38/chromosomes/chr22.fa.gz
```

### Alignment

``` shell
bowtie2-build chr22.fa.gz hg38_chr22
bowtie2 -x hg38_chr22 -1 SRR891268_R1.fastq.gz -2 SRR891268_R2.fastq.gz | \
    samtools sort -n - > SRR891268.bam
```

### Peak calling

``` {.bash .shell}
Genrich -t SRR891268.bam -o SRR891268.narrowPeak \
    -e "chrM" -f SRR891268.log -m 30 -j\
    -a 20 -r \
    -k SRR891268.bg
```

ATAC-seq is epigenomics NGS technique to identify open chromatin. Since the sequencing is in a paired-end mode, both reads have to be indicated for `bowtie2`.

-   `-1` indicates the forward read,

-   while `-2` indicates the reverse read. 

`Genrich` has an advantage over `MACS2` since it is offering to do the necessary filtering steps during peak calling.

-   `-e` allows to exclude chromosomes that are specified. In this case the mitochondrial chromosome (chrM).

-   `-m` filters low quality reads.

-   `-j` specifies that it has to run in the atac-seq mode.

-   `-a` is the minimum AUC for a peak.

-   `-r` removes PCR duplicates.

-   `-k` creates a bedgraph-ish file for pileups. 

#### Write the script into a Snakefile.

-   Create a new directory `atac-seq` in your home directory for this.

    ``` zsh
    cd
    mkdir atac-seq
    cd atac-seq
    ```

### Snakefile for the advanced example

``` python
rule all:
  input: "genrich/SRR891268.narrowPeak", "genrich/SRR891268.log", "genrich/SRR891268.bg"

rule download:
  output:
    read1 = "fastq/SRR891268_R1.fastq.gz",
    read2 = "fastq/SRR891268_R2.fastq.gz",
    reference = "reference/chr22.fa.gz"
  conda: "envs/wget.yaml"
  shell:
    """
    wget https://zenodo.org/record/3270536/files/SRR891268_R1.fastq.gz -O {output.read1}
    wget https://zenodo.org/record/3270536/files/SRR891268_R2.fastq.gz -O {output.read2}
    wget http://hgdownload.soe.ucsc.edu/goldenPath/hg38/chromosomes/chr22.fa.gz -O {output.reference}
    """


rule bowtie2:
  input:
    read1 = "fastq/SRR891268_R1.fastq.gz",
    read2 = "fastq/SRR891268_R2.fastq.gz",
    reference = "reference/chr22.fa.gz"
  output: "bowtie2/SRR891268.bam"
  conda: "envs/bowtie2.yaml"
  shell:
    """
    bowtie2-build {input.reference} reference/hg38_chr22
    bowtie2 -x reference/hg38_chr22 -1 {input.read1} -2 {input.read2} | \
    samtools sort -n - > {output}
    """

rule genrich:
  input: "bowtie2/SRR891268.bam"
  output:
    peaks = "genrich/SRR891268.narrowPeak",
    log = "genrich/SRR891268.log",
    bedgraph = "genrich/SRR891268.bg"
  params:
    exclude = "chrM",
    mapq = "30",
    minauc = "20"
  conda: "envs/genrich.yaml"
  shell:
    """
    Genrich -t {input} -o {output.peaks} \
    -e {params.exclude} -f {output.log} -m {params.mapq} -j \
    -a {params.minauc} -r -k {output.bedgraph}
    """
```
