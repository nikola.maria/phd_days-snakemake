rule all:
  input: "genrich/SRR891268.narrowPeak", "genrich/SRR891268.log", "genrich/SRR891268.bg"

rule download:
  output:
    read1 = "fastq/SRR891268_R1.fastq.gz",
    read2 = "fastq/SRR891268_R2.fastq.gz",
    reference = "reference/chr22.fa.gz"
  conda: "envs/wget.yaml"  
  shell:
    """
    wget https://zenodo.org/record/3270536/files/SRR891268_R1.fastq.gz -O {output.read1}
    wget https://zenodo.org/record/3270536/files/SRR891268_R2.fastq.gz -O {output.read2}
    wget http://hgdownload.soe.ucsc.edu/goldenPath/hg38/chromosomes/chr22.fa.gz -O {output.reference}
    """


rule bowtie2:
  input:
    read1 = "fastq/SRR891268_R1.fastq.gz",
    read2 = "fastq/SRR891268_R2.fastq.gz",
    reference = "reference/chr22.fa.gz"
  output: "bowtie2/SRR891268.bam"
  conda: "envs/bowtie2.yaml"
  shell:
    """
    bowtie2-build {input.reference} reference/hg38_chr22
    bowtie2 -x reference/hg38_chr22 -1 {input.read1} -2 {input.read2} | \
    samtools sort -n - > {output}
    """

rule genrich:
  input: "bowtie2/SRR891268.bam"
  output: 
    peaks = "genrich/SRR891268.narrowPeak",
    log = "genrich/SRR891268.log",
    bedgraph = "genrich/SRR891268.bg"
  params:
    exclude = "chrM",
    mapq = "30",
    minauc = "20"
  conda: "envs/genrich.yaml"
  shell: 
    """
    Genrich -t {input} -o {output.peaks} \
    -e {params.exclude} -f {output.log} -m {params.mapq} -j \
    -a {params.minauc} -r -k {output.bedgraph}
    """

